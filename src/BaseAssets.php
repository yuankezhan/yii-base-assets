<?php

namespace yuankezhan\yiiBaseAssets;
use yii\web\AssetBundle;


class BaseAssets extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = __DIR__ . '/assets';
    public $cssOptions = ['position' => \yii\web\View::POS_BEGIN];
    public $jsOptions = ['position' => \yii\web\View::POS_BEGIN];

    /**
     * {@inheritdoc}
     */
    public $css = [
//        'css/iconfont.css',
    ];

    /**
     * {@inheritdoc}
     */
    public $js = [
        'js/jquery-3.6.0.min.js'
    ];
}